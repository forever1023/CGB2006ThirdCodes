package com.cy.java.basic;
public class TestVarArgs {
//	static void delete(int id) {}
//	static void delete(int id1,int id2) {}
//	static void delete(int id1,int id2,int id3) {}
	//.....
	//可变参数可以看成是一个特殊数组,目的是简化类中方法名相同,
	//参数类型相同,但参数个数不同的这样的一些重载方法定义
	static void delete(int... ids) {
		System.out.println(ids.length);
		if(ids.length>0)System.out.println(ids[0]);
	}//可变参数应用
	public static void main(String[] args) {
		delete();
		delete(10);
		delete(10,20);
		delete(10,20,30);
	}
}
