package com.cy.java.oop;
//学过断点调试吗
//断点常用快捷键?
//1)f8 当前断点运行结束,进入下一个断点.没有下一个断点就直接运行结束.
//2)f6 单步调试
//3)f5 进入方法内部
//4)f7 从方法内部出来
public class TestIntegerCache01 {
	public static void main(String[] args) {
		Integer t1=100;//"="右边会有编译时优化,将右边的语句转换为Integer.valueOf(100)
		Integer t2=100;//从池中取
		Integer t3=200;//new Integer(200);
		Integer t4=200;//new Integer(200)
		System.out.println(t1==t2);//true
		System.out.println(t3==t4);//false
	}
}
