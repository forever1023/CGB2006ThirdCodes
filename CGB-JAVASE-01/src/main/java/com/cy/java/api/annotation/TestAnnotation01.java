package com.cy.java.api.annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
/**自定义注解
 * @Retention 用于描述注解何时有效,例如RUNTIME就表示运行时有效
 * @Target 用于描述注解可以描述哪些对象?例如Type表示注解可以描述类或接口
 * */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface Mapper{}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface Select{
	String value();
}

@Mapper
interface GoodsDao{
	@Select("select * from tb_goods")
	Object findGoods();
}

public class TestAnnotation01 {
	public static void main(String[] args) throws Exception {
		//1.如何判定GoodsDao接口上是否有@Mapper注解
		Class<?> cls=GoodsDao.class;//获得接口的字节码对象
		boolean flag=cls.isAnnotationPresent(Mapper.class);
		System.out.println(flag);
		Mapper mapper=cls.getDeclaredAnnotation(Mapper.class);
		if(mapper!=null) {
			System.out.println("GoodDao接口上有mapper注解");
		}
		//2.获取GoodsDao中findGoods方法上的@Select注解中的SQL?
		Method method=cls.getDeclaredMethod("findGoods");
		Select select=method.getDeclaredAnnotation(Select.class);
		String sql=select.value();
		System.out.println(sql);
	}
}
