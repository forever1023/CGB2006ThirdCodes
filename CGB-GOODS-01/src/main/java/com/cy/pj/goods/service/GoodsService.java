package com.cy.pj.goods.service;
import java.util.List;
import com.cy.pj.goods.pojo.Goods;

/**
 *  商品模块业务接口:
 *  负责:
 * 1)核心业务
 * 2)拓展业务(记录日志,缓存,事务控制,权限控制,...)
 */
public interface GoodsService {
	
      Goods findById(Integer id);
      
      int updateGoods(Goods goods);
	  int saveGoods(Goods goods);
      /**
       * 	查询所有商品信息
       * @return
       */
	  List<Goods> findGoods();
	  
	  int deleteById(Integer id);
	  
}
