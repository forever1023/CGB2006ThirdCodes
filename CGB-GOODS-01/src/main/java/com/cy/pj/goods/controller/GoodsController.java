package com.cy.pj.goods.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;

@Controller
@RequestMapping("/goods/")
public class GoodsController {

	 @Autowired
	 private GoodsService goodsService;
	 
	 @RequestMapping("doGoodsAddUI")
	 public String doGoodsAddUI() {
		 return "goods-add";
	 }
	 
	 @RequestMapping("doFindById/{id}")
	 public String doFindById(@PathVariable Integer id,Model model) {
		 Goods goods=goodsService.findById(id);
		 model.addAttribute("goods",goods);
		 return "goods-update";//此view由谁解析?ViewResolver(ThymeleafViewResolver)
		 //这里的ThymeleafViewResolver做了什么?
		 //1)在viewname的基础上添加前缀和后缀(/templates/pages/goods-update.html),并找到对应的view(真正的页面对象)
		 //2)将model中的数据取出,然后填充到view上(/templates/pages/goods-update.html)
		 //3)将view交给DispatcherServlet
	 }
	 
	 @RequestMapping("doUpdateGoods")
	 public String doUpdateGoods(Goods goods) {
		 goodsService.updateGoods(goods);
		 return "redirect:/goods/doGoodsUI";
	 }
	 
	 @RequestMapping("doSaveGoods")
	 public String doSaveGoods(Goods goods) {
		 goodsService.saveGoods(goods);
		 return "redirect:/goods/doGoodsUI";
	 }
	 
	 @RequestMapping("doDeleteById/{id}")
	 public String doDeleteById(@PathVariable Integer id) {
		 goodsService.deleteById(id);
		 return "redirect:/goods/doGoodsUI";
	 }
	 
	 @RequestMapping("doGoodsUI")
	 public String doFindGoods(Model model)throws Exception {
		 //Thread.sleep(7000);
		 List<Goods> list=goodsService.findGoods();
		 model.addAttribute("list", list);
		 return "goods";
	 }
	 
}
