package com.cy.pj.sys.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 	基于此Controller处理所有页面请求
 */
@RequestMapping("/")
@Controller
public class PageController {
	//http://localhost/log/log_list
	//http://localhost/menu/menu_list
	//rest风格的url,{}代表是变量表达式
	
	@RequestMapping("{module}/{moduleUI}")
	public String doModuleUI(@PathVariable String moduleUI) {
		return "sys/"+moduleUI;
	}
	
//	@RequestMapping("menu/menu_list")
//	public String doMenuUI() {
//		return "sys/menu_list";
//	}
//
//	@RequestMapping("log/log_list")
//	public String doLogUI() {
//		return "sys/log_list";
//	}
	

	@RequestMapping("doPageUI")
	public String doPageUI() {
		return "common/page";
	}

	@RequestMapping("doIndexUI")
	public String doIndexUI() {
		return "starter";//ThymeleafViewResolver
	}
	
	
	
}
