package com.cy.pj.sys.service.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.common.utils.AssertUtils;
import com.cy.pj.sys.dao.SysUserDao;
import com.cy.pj.sys.dao.SysUserRoleDao;
import com.cy.pj.sys.pojo.SysUser;
import com.cy.pj.sys.pojo.SysUserDept;
import com.cy.pj.sys.service.SysUserService;

@Service
public class SysUserServiceImpl implements SysUserService {

	@Autowired
	private SysUserDao sysUserDao;
	
	@Autowired
	private SysUserRoleDao sysUserRoleDao;
	
	@Override
	public Map<String, Object> findObjectById(Integer id) {
		//1.参数校验
		AssertUtils.isArgValid(id==null||id<1,"id值无效");
		//2.查询用户以及用户对应的部门信息
		SysUserDept user=sysUserDao.findObjectById(id);
		AssertUtils.isServiceValid(user==null, "记录可能已经不存在");
		//3.查询用户对应的角色信息
		List<Integer> roleIds=sysUserRoleDao.findRoleIdsByUserId(id);
		//4.封装查询结果
		Map<String,Object> map=new HashMap<>();
		map.put("user", user);
		map.put("roleIds", roleIds);
		return map;
	}
	
	@Override
	public int saveObject(SysUser entity, Integer[] roleIds) {
		//1.参数校验
		AssertUtils.isArgValid(entity==null, "保存对象不能为空");
		AssertUtils.isArgValid(entity.getUsername()==null||"".equals(entity.getUsername()), "用户名不能为空");
		AssertUtils.isArgValid(entity.getPassword()==null||"".equals(entity.getPassword()), "密码不能为空");
		AssertUtils.isArgValid(roleIds==null||roleIds.length==0, "必须为用户分配角色");
		//2.保存用户自身信息
		//2.1 构建salt(盐)值
		String salt=UUID.randomUUID().toString();//产生一个固定长度随机字符串
		//2.2 基于相关API对密码进行加密
		//借助spring框架中自带API对密码进行加密
		//String hashedPassword=
		//DigestUtils.md5DigestAsHex((salt+entity.getPassword()).getBytes());
		//借助shiro框架中的API对密码进行加密
		SimpleHash sh=new SimpleHash("MD5",//算法名称
				          entity.getPassword(), //未加密的密码
				          salt,//加密盐 
				          1);//这里1表示加密次数
		String hashedPassword=sh.toHex();//将加密结果转换为16进制的字符串
		entity.setSalt(salt);//为什么盐值也要保存到数据库?(登录时还要使用此salt对登录密码进行加密)
		entity.setPassword(hashedPassword);
		//2.3 将用户信息写入到数据库
		int rows=sysUserDao.insertObject(entity);
		//3.保存用户与角色关系数据
		sysUserRoleDao.insertObjects(entity.getId(), roleIds);
		return rows;
	}
	
	@Override
	public int updateObject(SysUser entity, Integer[] roleIds) {
		//1.参数校验
		AssertUtils.isArgValid(entity==null, "保存对象不能为空");
		AssertUtils.isArgValid(entity.getUsername()==null||"".equals(entity.getUsername()), "用户名不能为空");
		AssertUtils.isArgValid(roleIds==null||roleIds.length==0, "必须为用户分配角色");
		//2.保存用户自身信息
		int rows=sysUserDao.updateObject(entity);
		AssertUtils.isServiceValid(rows==0, "记录可能已经不存在了");
		//3.保存用户与角色关系数据
		sysUserRoleDao.deleteObjectsByUserId(entity.getId());
		sysUserRoleDao.insertObjects(entity.getId(), roleIds);
		return rows;
	}
	
	@Override
	public int validById(Integer id, Integer valid) {
		//1.参数校验
		AssertUtils.isArgValid(id==null||id<1, "参数值无效");
		AssertUtils.isArgValid(valid!=0&&valid!=1, "状态值无效");
		//2.修改用户状态
		int rows=sysUserDao.validById(id, valid,"admin");//这里的admin先假设为登录用户
		AssertUtils.isServiceValid(rows==0, "记录可能已经不存在");
		return rows;
	}
	
	@Override
	public PageObject<SysUserDept> findPageObjects(String username, Long pageCurrent) {
		//1.参数有效性校验
		if(pageCurrent==null||pageCurrent<1)
			throw new IllegalArgumentException("页码值无效");
		//2.查询总记录数并校验
		long rowCount=sysUserDao.getRowCount(username);
		if(rowCount==0)
			throw new ServiceException("没有找到对应记录");
		//3.查询当前页记录
		int pageSize=2;
		long startIndex=(pageCurrent-1)*pageSize;
		List<SysUserDept> records=
				sysUserDao.findPageObjects(username, startIndex, pageSize);
		//4.封装查询结果并返回
		return new PageObject<>(rowCount, records, pageSize, pageCurrent);
	}

}
