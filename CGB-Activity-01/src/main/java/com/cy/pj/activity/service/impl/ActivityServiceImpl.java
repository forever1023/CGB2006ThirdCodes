package com.cy.pj.activity.service.impl;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.pj.activity.dao.ActivityDao;
import com.cy.pj.activity.pojo.Activity;
import com.cy.pj.activity.service.ActivityService;

@Service
public class ActivityServiceImpl implements ActivityService {

	@Autowired
	private ActivityDao activityDao;
	
	@Override
	public int saveObject(Activity entity) {
		// TODO Auto-generated method stub
		//System.out.println("entity.insert.before.id="+entity.getId());
		int rows=activityDao.insertObject(entity);
		//System.out.println("entity.insert.after.id="+entity.getId());
		//希望时间到了(endTime)自动修改活动状态
		//解决方案:基于任务调度去实现(任务调度-基于时间的设计自动执行任务)
		//代码方案:
		//1)借助Java中的Timer对象去实现
		Timer timer=new Timer();//此对象创建时会在底层启动一个线程,通过此线程对时间进行监控
		//2)执行任务(任务类型为TimerTask类型)
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				//在此位置修改活动的状态信息
				System.out.println("开始执行活动状态的修改操作");
				activityDao.updateState(entity.getId());
				timer.cancel();
			}
		}, entity.getEndTime());
		//2)借助Java线程池中的任务调度对象(ScheduledExecutorService )去实现
		//3)借助第三方框架去实现(quartz)
		return rows;
	
	}
	
	@Override
	public List<Activity> findActivitys() {
		//将来要进行缓存设计,记录日志,....
		return activityDao.findActivitys();
	}

}
