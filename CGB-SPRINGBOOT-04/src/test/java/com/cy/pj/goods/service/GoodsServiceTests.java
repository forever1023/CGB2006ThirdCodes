package com.cy.pj.goods.service;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.cy.pj.goods.pojo.Goods;


@SpringBootTest
public class GoodsServiceTests {

	//has a
	@Autowired 
	private GoodsService goodsService;
	
	@Test
	void testFindGoods() {
		List<Goods> list=goodsService.findGoods();
		for(Goods g:list) {
			System.out.println(g);
		}
	}
	
	@Test
	void testDeleteById() {
		int rows=goodsService.deleteById(10);
		System.out.println("delete.rows="+rows);
	}
}
