package com.cy.pj.goods.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
@SpringBootTest
public class GoodsDaoTests {
	 /**关联数据层接口,并由spring为其进行值的注入
	  * FAQ?
	  * 1)goodsDao 指向的对象是谁?由谁创建?由管理?
	  * 2)goodsDao 指向的对象内部会做什么事情?基于Mybatis API进行会话操作
	  * */
	 @Autowired
	 private GoodsDao goodsDao;//has a,FAQ?
	 
	 @Test
	 void testDeleteById() {
		 int rows=goodsDao.deleteById(1);
		 System.out.println("rows="+rows);
	 }
	 
	 @Test
	 void testDeleteObjects() {
		 int rows=goodsDao.deleteObjects(10,20);
		 System.out.println("rows="+rows);
	 }
}



