package com.cy.pj.common.datasource;
import java.sql.Connection;

import javax.sql.DataSource;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DataSourceTests {
	//FAQ?
	//请问dataSource对象在运行时指向的具体对象类型是什么?你怎么知道的?
	//请问dataSource对象是谁来帮你创建的?Spring框架(基于底层的自动配置-DataSourceAutoConfiguration)
	@Autowired
	private DataSource dataSource;
	
	/**测试通过数据源DataSource对象获取一个连接*/
	@Test
	void testGetConnection() throws Exception{
		//com.zaxxer.hikari.HikariDataSource
		//输出dataSource变量指向的对象的类型
		//System.out.println(dataSource.getClass().getName());
		//请问这个获取连接的过程你了解吗?了解
		//第一次获取连接时首先会检测连接池是否存在,假如不存在则创建并初始化池.
		//然后基于Driver对象创建与数据库的链接,并将链接放入池中.
		//最后从池中返回用户需要的链接
		Connection conn=dataSource.getConnection();
		System.out.println(conn);
	}
	
}
